#!/bin/bash
python3 -m virtualenv venv --no-site-packages -p python3
source venv/bin/activate

parentdir="$(dirname $(readlink -f "$0"))"
pip install -r "$parentdir/../src/python/requirements.txt"
