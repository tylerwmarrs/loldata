#!/bin/bash
parentdir="$(dirname $(readlink -f "$0"))"


for file in $parentdir/../src/go/*.go; do
    o="$(basename $file .go)"
    echo $o
    go build -o $parentdir/$o $file
done
