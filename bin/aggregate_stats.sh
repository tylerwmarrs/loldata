#!/bin/bash

parentdir="$(dirname $(readlink -f "$0"))"
export PATH="$PATH:$parentdir"

source $parentdir/../venv/bin/activate
python3 $parentdir/../src/python/aggregate_stats.py "$@"
