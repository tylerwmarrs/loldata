#!/bin/bash

parentdir="$(dirname $(readlink -f "$0"))"
export PATH="$PATH:$parentdir"

source $parentdir/../venv/bin/activate

now=$(date +"%m_%d_%Y")
workdir="/home/tyler/baronthrows/$now"
snakemake -s $parentdir/../src/python/regional_baron_throws.snake --directory $workdir "$@"
