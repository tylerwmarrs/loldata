"""
Reads results from both the regions and throws csv files and aggregates the
results. The results are then dumped to the desired JSON file.

The end result is a composition of summoner throw rates by region. Each
region result object has a region code key. Each player result has a
summoner id key.
-------
Example
-------
{
    "created": "some date format",
    "season": "SEASON2016",
    "regions": [
        {
            "name": "North America",
            "code": "na"
        }
        ...more regions
    ],
    "results": {
        "na": [
            {
                "133131": {
                    {
                        "summoner": "SummonerName",
                        "matches": 153,
                        "throws": 18,
                        "throwRate": 0.1176470588235294
                    }
                }
            }
        ],
        "euw": [
            ...player data
        ]
        ...more player results by region
    }
}
"""
from datetime import datetime
import argparse
import json

# Patch json encoder to handler datetime objects
json.JSONEncoder.default = lambda self,obj: (obj.isoformat() if isinstance(obj, datetime) else None)


def read_regions(region_file):
    """
    Reads a comma seperated regions file in the format:
    region,code

    The first line is skipped and is assumed to be the header line.
    """
    regions = []
    with open(region_file, "r") as f:
        # skip first line
        f.readline()
        for line in f.readlines():
            data = line.strip().split(",")
            regions.append({
                "name": data[0].strip(),
                "code": data[1].strip()
            })

    return regions


def read_throws(throws_file):
    """
    Reads a comma seperated throws file in the format:
    matchId,summonerId,summonerName,season,region,isThrow

    The first line is skipped and is assumed to be the header line.
    This function is intended to be used as a generator for processing
    the results. A map of the data is returned.
    """
    with open(throws_file, "r") as f:
        # skip first line
        f.readline()
        for line in f.readlines():
            data = line.strip().split(",")
            if len(data) == 6:
                yield {
                    "region": data[0].strip(),
                    "season": data[1].strip(),
                    "match_id": data[2].strip(),
                    "summoner_id": data[3].strip(),
                    "summoner_name": data[4].strip(),
                    "is_throw": data[5].strip()
                }


def str_to_bool(s):
    """
    Helper to convert string to a boolean.
    """
    return s.lower() in ("true", "1", "t")


def write_results_to_file(results, file_path):
    """
    Writes the given results to given file path.
    """
    with open(file_path, "w") as f:
        json.dump(results, f)


def main():
    description = """
    Process baron throw data by region and output results in JSON to output
    file provided.
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-r", "--regionsFile", required=True)
    parser.add_argument("-t", "--throwsFile", required=True)
    parser.add_argument("-o", "--outFile", required=True)

    args = parser.parse_args()

    regions = read_regions(args.regionsFile)

    # Aggregate player throw rate per region
    results = {}
    season = ""
    for result in read_throws(args.throwsFile):
        # Season will always be the same for all summoners
        season = result["season"]

        # Add region if it does not exist yet
        region = result["region"]
        if region not in results:
            results[region] = {}

        # Add summoner if they do not exist
        summoner_id = result["summoner_id"]
        if summoner_id not in results[region]:
            summoner_name = result["summoner_name"]
            results[region][summoner_id] = {
                "summoner": summoner_name,
                "matches": 0,
                "throws": 0,
                "throwRate": 0
            }

        # Found a match so increment
        results[region][summoner_id]["matches"] += 1

        # Check for a throw and compute throw rate
        if str_to_bool(result["is_throw"]):
            results[region][summoner_id]["throws"] += 1
            matches = results[region][summoner_id]["matches"]
            throws = results[region][summoner_id]["throws"]
            throwRate = throws / matches
            results[region][summoner_id]["throwRate"] = throwRate

    output = {
        "created": datetime.now(),
        "season": season,
        "regions": regions,
        "results": results
    }

    write_results_to_file(output, args.outFile)


if __name__ == "__main__":
    main()
