package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"lolapi"
)

// Center x/y values of baron nashor
const BARON_X int64 = 5007
const BARON_Y int64 = 10471

// Radius around baron nashor considered the "baron zone"
const BARON_R int64 = 1947

// Seconds to keep track of kill events before baron was killed
const BEFORE_THRESHOLD int64 = 30

// Seconds to keep track of kill events after baron was killed
const AFTER_THRESHOLD int64 = 30

// Container for a baron event and associated kill events within the before
// and after thresholds
type BaronEvent struct {
	FrameIndex int
	Timestamp  int64
	KillerID   int64
	KillEvents []KillEvent
}

// Keeps track of who killed who, where they were killed and the time they died
type KillEvent struct {
	Timestamp int64
	KillerID  int64
	VictimID  int64
	XCoord    int64
	YCoord    int64
}

// Stores matches to process
type MatchRequest struct {
	SummonerID string
	Summoner   string
	Season     string
	Region     string
	MatchID    string
	LolKey     string
}

// Stores results of matches
type MatchResult struct {
	Request      MatchRequest
	IsBaronThrow bool
}

// Determine what team the parcipant is on. Right now the API has a scale of
// 1 - 10 for IDs. 1-5 = blue, 6-10 = red
func teamForParticipantID(x int64) string {
	if x >= 1 && x <= 5 {
		return "blue"
	}

	if x == 0 {
		return ""
	}

	return "red"
}

// Provides new instance of BaronEvent
func NewBaronEvent() *BaronEvent {
	be := new(BaronEvent)
	be.KillEvents = make([]KillEvent, 0)
	return be
}

// Adds a KillEvent
func (be *BaronEvent) AddKillEvent(k KillEvent) {
	be.KillEvents = append(be.KillEvents, k)
}

// Helper to see if a x,y coord exists within the area of the baron zone
func killInBaronZone(x, y int64) bool {
	dx := BARON_X - x
	dy := BARON_Y - y
	squareDist := (dx * dx) + (dy * dy)
	squareR := BARON_R * BARON_R
	return squareDist <= squareR
}

// Convert milliseconds to minutes
func msToM(ms int64) int64 {
	return ms / 1000 / 60
}

// Convert milliseconds to seconds
func msToS(ms int64) int64 {
	return ms / 1000
}

// Check if match contains any baron kills
func baronKilled(match lolapi.MatchDetailDto) bool {
	return match.Teams[0].BaronKills > 0 ||
		match.Teams[1].BaronKills > 0
}

// Check if match has any baron throws in it
func isBaronThrow(match lolapi.MatchDetailDto, summonerId string) bool {
	if !baronKilled(match) {
		return false
	}
	// find the summoner's team
	summonerTeam := ""
	for _, p := range match.ParticipantIdentities {
		testSummonerId := fmt.Sprintf("%d", p.Player.SummonerID)
		if testSummonerId == summonerId {
			summonerTeam = teamForParticipantID(p.ParticipantID)
			break
		}
	}

	// Find and keep track of all baron events.
	baronEvents := make([]*BaronEvent, 0)
	for i, f := range match.Timeline.Frames {
		// Baron doesn't spawn until 20 minutes in game so we can skip events
		// earlier events.
		minute := msToM(f.Timestamp)
		if minute < 20 {
			continue
		}

		for _, e := range f.Events {
			if e.MonsterType == "BARON_NASHOR" {
				be := NewBaronEvent()
				be.FrameIndex = i
				be.Timestamp = e.Timestamp
				be.KillerID = e.KillerID
				baronEvents = append(baronEvents, be)
			}
		}
	}

	// Loop over found baron events and find kill events within given time thresholds.
	// We store the kill events found within the threshold.
	for _, be := range baronEvents {

		// Figure out whihc frame index to start and stop at.
		// Each frame specifies a minute interval in time.
		// We only care about events within a short interval of the baron
		// so we only start at one frame before and after.
		startFrameIndex := be.FrameIndex - 1
		endFrameIndex := be.FrameIndex + 1
		if endFrameIndex > len(match.Timeline.Frames) {
			endFrameIndex = len(match.Timeline.Frames) - 1
		}

		for _, f := range match.Timeline.Frames[startFrameIndex:endFrameIndex] {
			for _, e := range f.Events {
				if e.EventType == "CHAMPION_KILL" {
					if e.Timestamp < be.Timestamp {
						secsBefore := msToS(be.Timestamp - e.Timestamp)
						if secsBefore <= BEFORE_THRESHOLD {
							be.AddKillEvent(KillEvent{e.Timestamp, e.KillerID, e.VictimID, e.Position.X, e.Position.Y})
						}
					} else {
						secsAfter := msToS(e.Timestamp - be.Timestamp)
						if secsAfter <= AFTER_THRESHOLD {
							be.AddKillEvent(KillEvent{e.Timestamp, e.KillerID, e.VictimID, e.Position.X, e.Position.Y})
						}
					}
				}
			}
		}
	}

	// Determine if any baron events has "near aces" or aces in them within the
	// baron zone.
	// Essentially - 4 or 5 deaths on either team.
	isThrow := false
	for _, be := range baronEvents {
		blueDeaths := 0
		redDeaths := 0
		for _, ke := range be.KillEvents {
			if killInBaronZone(ke.XCoord, ke.YCoord) {
				victim := teamForParticipantID(ke.VictimID)
				if victim == "blue" {
					blueDeaths++
				} else {
					redDeaths++
				}
			}
		}

		if (summonerTeam == "blue" && blueDeaths >= 4) ||
			(summonerTeam == "red" && redDeaths >= 4) {
			isThrow = true
			break
		}
	}

	return isThrow
}

// Worker that processes matches
func matchWorker(jobs <-chan MatchRequest, results chan<- MatchResult, wg *sync.WaitGroup) {
	defer wg.Done()

	for j := range jobs {
		api := lolapi.NewLolApi(j.LolKey, j.Region)
		match, rerr := api.Match(j.MatchID, true)

		result := MatchResult{}
		result.Request = j

		if rerr != nil {
			// handle 404
			if rerr.StatusCode == 404 {
				continue
			}

			// handle rate limiting response
			for (rerr != nil &&
                (rerr.StatusCode == 429 || rerr.StatusCode > 500)) {
				for {
					time.Sleep(time.Second)
					if api.ApiAvailable() {
						break
					}
				}
				match, rerr = api.Match(j.MatchID, true)
			}
		}

        if match != nil {
            result.IsBaronThrow = isBaronThrow(*match, j.SummonerID)
            results <- result
        }
	}
}

// fetch challenger data and handle rate limiting...
func fetchChallengers(lolKey, region string) *lolapi.LeagueDto {
	api := lolapi.NewLolApi(lolKey, region)
	challengers, rerr := api.ChallengerSolo()

	if rerr != nil {

		// handle rate limiting response
		for (rerr != nil &&
            (rerr.StatusCode == 429 || rerr.StatusCode > 500)) ||
            challengers == nil {
			for {
				time.Sleep(time.Second)
				if api.ApiAvailable() {
					break
				}
			}
			challengers, rerr = api.ChallengerSolo()
		}
	}

	return challengers
}

// fetch ranked matches for summoner and handle rate limiting...
func fetchMatches(lolKey, region, season, playerID string) *lolapi.MatchListDto {
	matchOpts := make(map[string]string)
	matchOpts["seasons"] = season

	api := lolapi.NewLolApi(lolKey, region)
	matches, rerr := api.SummonerRankedMatches(playerID, matchOpts)

	if rerr != nil {

		// handle rate limiting response
		for (rerr != nil &&
            (rerr.StatusCode == 429 || rerr.StatusCode > 500)) ||
            matches == nil {
			for {
				time.Sleep(time.Second)
				if api.ApiAvailable() {
					break
				}
			}
			matches, rerr = api.SummonerRankedMatches(playerID, matchOpts)
		}
	}

	return matches
}

// Loops over given regions to obtain challenger list and further
// queries each summoner's match history
func generateMatchRequests(jobs chan<- MatchRequest, regions, lolKey, season string) {
	for _, region := range strings.Split(regions, ",") {
		cleanRegion := strings.TrimSpace(region)
		// request challenger data
		challengers := fetchChallengers(lolKey, cleanRegion)
		for _, entry := range challengers.Entries {
			playerID := entry.ID
			playerName := entry.Name

			// request matches
			matches := fetchMatches(lolKey, region, season, playerID)
			for _, match := range matches.Matches {
				request := MatchRequest{}
				request.SummonerID = playerID
				request.Summoner = playerName
				request.Season = season
				request.Region = region
				request.MatchID = fmt.Sprintf("%d", match.MatchID)
				request.LolKey = lolKey

				jobs <- request
			}

		}
	}
    close(jobs)
}

// Header line for output file
func getHeaderLine() string {
	return "matchId,summonerId,summonerName,season,region,isThrow"
}

// Converts a MatchResult struct to a line for the output file
func getMatchLine(result MatchResult) string {
	var buffer bytes.Buffer
	buffer.WriteString(result.Request.MatchID)
	buffer.WriteString(",")
	buffer.WriteString(result.Request.SummonerID)
	buffer.WriteString(",")
	buffer.WriteString(result.Request.Summoner)
	buffer.WriteString(",")
	buffer.WriteString(result.Request.Season)
	buffer.WriteString(",")
	buffer.WriteString(result.Request.Region)
	buffer.WriteString(",")
	buffer.WriteString(strconv.FormatBool(result.IsBaronThrow))

	return buffer.String()
}

// Write all match results to given file
func writeResultsToFile(results <-chan MatchResult, filePath string) {
	fileHandle, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}

	writer := bufio.NewWriter(fileHandle)
	defer fileHandle.Close()

	fmt.Fprintln(writer, getHeaderLine())
	for v := range results {
		fmt.Fprintln(writer, getMatchLine(v))
	}
	writer.Flush()
}

func main() {
	// parse command line options
	regions := ""
	lolKey := ""
	season := ""
	output := ""
	threads := 1

	flag.StringVar(&regions, "regions", "na", "Region codes separated by comma")
	flag.StringVar(&lolKey, "lolkey", "", "Riot API key")
	flag.StringVar(&season, "season", "", "Season to aggregate data for")
	flag.StringVar(&output, "output", "", "Output file to write to")
	flag.IntVar(&threads, "threads", 1, "Number of threads to use")
	flag.Parse()

	if lolKey == "" || season == "" || output == "" {
		flag.Usage()
		os.Exit(1)
	}

	// set up channels
	jobs := make(chan MatchRequest, 100)
	results := make(chan MatchResult, 100)

	// create some workers
	wg := new(sync.WaitGroup)
	for w := 0; w <= threads; w++ {
		wg.Add(1)
		go matchWorker(jobs, results, wg)
	}

	// generate some jobs
    go generateMatchRequests(jobs, regions, lolKey, season)

	// wait for workgroup to finish and close the result channel
	go func() {
		wg.Wait()
		close(results)
	}()

	// write out the results to a file
	writeResultsToFile(results, output)
}
