package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"lolapi"
)

// Request to get matches
type MatchesRequest struct {
	Region     string
	Season     string
	LolKey     string
	SummonerID string
	Summoner   string
}

// Stores matches to process
type MatchResult struct {
	Region     string
	Season     string
	MatchID    string
	SummonerID string
	Summoner   string
}

// fetch ranked matches for summoner and handle rate limiting...
func fetchMatchesWorker(jobs <-chan MatchesRequest, results chan<- MatchResult, wg *sync.WaitGroup) {
	defer wg.Done()

	for job := range jobs {
		matchOpts := make(map[string]string)
		matchOpts["seasons"] = job.Season

		api := lolapi.NewLolApi(job.LolKey, job.Region)
		matches, rerr := api.SummonerRankedMatches(job.SummonerID, matchOpts)

		if rerr != nil {

			// handle rate limiting response
			for (rerr != nil &&
				(rerr.StatusCode == 429 || rerr.StatusCode > 500)) ||
				matches == nil {
				for {
					time.Sleep(time.Second)
					if api.ApiAvailable() {
						break
					}
				}
				matches, rerr = api.SummonerRankedMatches(job.SummonerID, matchOpts)
			}
		}

		for _, match := range matches.Matches {
			result := MatchResult{}
			result.SummonerID = job.SummonerID
			result.Summoner = job.Summoner
			result.Season = job.Season
			result.Region = job.Region
			result.MatchID = fmt.Sprintf("%d", match.MatchID)

			results <- result
		}
	}
}

// Reads input file and generates requests
func generateJobs(jobs chan<- MatchesRequest, input, lolKey, season string) {
	// Open the file.
	f, err := os.Open(input)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
    first := true
	for scanner.Scan() {
        if first {
            first = false
            continue
        }
		line := strings.TrimSpace(scanner.Text())
		data := strings.Split(line, ",")

		request := MatchesRequest{}
		request.Region = strings.TrimSpace(data[0])
		request.Season = season
		request.LolKey = lolKey
		request.SummonerID = strings.TrimSpace(data[1])
		request.Summoner = strings.TrimSpace(data[2])

		jobs <- request
	}
	close(jobs)
}

// Header line for output file
func getHeaderLine() string {
	return "region,season,matchId,summonerId,summonerName"
}

// Converts a MatchResult struct to a line for the output file
func getMatchLine(result MatchResult) string {
	var buffer bytes.Buffer
	buffer.WriteString(result.Region)
	buffer.WriteString(",")
	buffer.WriteString(result.Season)
	buffer.WriteString(",")
	buffer.WriteString(result.MatchID)
	buffer.WriteString(",")
	buffer.WriteString(result.SummonerID)
	buffer.WriteString(",")
	buffer.WriteString(result.Summoner)

	return buffer.String()
}

// Write all match results to given file
func writeResultsToFile(results <-chan MatchResult, filePath string) {
	fileHandle, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}

	writer := bufio.NewWriter(fileHandle)
	defer fileHandle.Close()

	fmt.Fprintln(writer, getHeaderLine())
	for v := range results {
		fmt.Fprintln(writer, getMatchLine(v))
	}
	writer.Flush()
}

func main() {
	// parse command line options
	lolKey := ""
	season := ""
	input := ""
	output := ""
	threads := 1

	flag.StringVar(&lolKey, "lolkey", "", "Riot API key")
	flag.StringVar(&season, "season", "", "Season to aggregate data for")
	flag.StringVar(&input, "input", "", "Challengers csv file")
	flag.StringVar(&output, "output", "", "Output file to write to")
	flag.IntVar(&threads, "threads", 1, "Number of threads to use")
	flag.Parse()

	if lolKey == "" || season == "" || output == "" || input == "" {
		flag.Usage()
		os.Exit(1)
	}

	// set up channels
	jobs := make(chan MatchesRequest, 100)
	results := make(chan MatchResult, 100)

	// create some workers
	wg := new(sync.WaitGroup)
	for w := 0; w <= threads; w++ {
		wg.Add(1)
		go fetchMatchesWorker(jobs, results, wg)
	}

	// generate some jobs
	go generateJobs(jobs, input, lolKey, season)

	// wait for workgroup to finish and close the result channel
	go func() {
		wg.Wait()
		close(results)
	}()

	// write out the results to a file
	writeResultsToFile(results, output)
}
