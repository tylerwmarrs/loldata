package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"os"
	"strings"
    "strconv"
	"sync"
	"time"

	"lolapi"
    "lollearn/classifiers"
)

// Request to get matches
type MatchRequest struct {
	Region     string
	Season     string
	MatchID    string
	LolKey     string
	SummonerID string
	Summoner   string
}

// Stores matches to process
type MatchResult struct {
	Region     string
	Season     string
	MatchID    string
	SummonerID string
	Summoner   string
    IsBaronThrow bool
}

// Worker that processes matches
func fetchMatchWorker(jobs <-chan MatchRequest, results chan<- MatchResult, wg *sync.WaitGroup) {
	defer wg.Done()

	for j := range jobs {
		api := lolapi.NewLolApi(j.LolKey, j.Region)
		match, rerr := api.Match(j.MatchID, true)

		if rerr != nil {
			// handle 404
			if rerr.StatusCode == 404 {
				continue
			}

			// handle rate limiting response
			for (rerr != nil &&
                (rerr.StatusCode == 429 || rerr.StatusCode > 500)) {
				for {
					time.Sleep(time.Second)
					if api.ApiAvailable() {
						break
					}
				}
				match, rerr = api.Match(j.MatchID, true)
			}
		}

        if match == nil {
            fmt.Println("Skipping match...", j)
            continue
        }

        result := MatchResult{}
        result.Region = j.Region
        result.Season = j.Season
        result.MatchID = j.MatchID
        result.SummonerID = j.SummonerID
        result.Summoner = j.Summoner
        result.IsBaronThrow = classifiers.MatchIsBaronThrow(*match, j.SummonerID)
        results <- result

	}
}

// Reads input file and generates requests
func generateJobs(jobs chan<- MatchRequest, input, lolKey string) {
	// Open the file.
	f, err := os.Open(input)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
    first := true
	for scanner.Scan() {
        if first {
            first = false
            continue
        }
		line := strings.TrimSpace(scanner.Text())
		data := strings.Split(line, ",")

		request := MatchRequest{}
		request.Region = strings.TrimSpace(data[0])
		request.Season = strings.TrimSpace(data[1])
		request.MatchID = strings.TrimSpace(data[2])
		request.SummonerID = strings.TrimSpace(data[3])
		request.Summoner = strings.TrimSpace(data[4])
		request.LolKey = lolKey

		jobs <- request
	}
	close(jobs)
}

// Header line for output file
func getHeaderLine() string {
	return "region,season,matchId,summonerId,summonerName,isBaronThrow"
}

// Converts a MatchResult struct to a line for the output file
func getMatchLine(result MatchResult) string {
	var buffer bytes.Buffer
	buffer.WriteString(result.Region)
	buffer.WriteString(",")
	buffer.WriteString(result.Season)
	buffer.WriteString(",")
	buffer.WriteString(result.MatchID)
	buffer.WriteString(",")
	buffer.WriteString(result.SummonerID)
	buffer.WriteString(",")
	buffer.WriteString(result.Summoner)
	buffer.WriteString(",")
	buffer.WriteString(strconv.FormatBool(result.IsBaronThrow))

	return buffer.String()
}

// Write all match results to given file
func writeResultsToFile(results <-chan MatchResult, filePath string) {
	fileHandle, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}

	writer := bufio.NewWriter(fileHandle)
	defer fileHandle.Close()

	fmt.Fprintln(writer, getHeaderLine())
	for v := range results {
		fmt.Fprintln(writer, getMatchLine(v))
	}
	writer.Flush()
}

func main() {
	// parse command line options
	lolKey := ""
	input := ""
	output := ""
	threads := 1

	flag.StringVar(&lolKey, "lolkey", "", "Riot API key")
	flag.StringVar(&input, "input", "", "Challengers csv file")
	flag.StringVar(&output, "output", "", "Output file to write to")
	flag.IntVar(&threads, "threads", 1, "Number of threads to use")
	flag.Parse()

	if lolKey == "" || output == "" || input == "" {
		flag.Usage()
		os.Exit(1)
	}

	// set up channels
	jobs := make(chan MatchRequest, 100)
	results := make(chan MatchResult, 100)

	// create some workers
	wg := new(sync.WaitGroup)
	for w := 0; w <= threads; w++ {
		wg.Add(1)
		go fetchMatchWorker(jobs, results, wg)
	}

	// generate some jobs
	go generateJobs(jobs, input, lolKey)

	// wait for workgroup to finish and close the result channel
	go func() {
		wg.Wait()
		close(results)
	}()

	// write out the results to a file
	writeResultsToFile(results, output)
}
