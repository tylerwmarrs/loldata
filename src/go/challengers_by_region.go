package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"os"
	"strings"
	"time"

	"lolapi"
)

// fetch challenger data and handle rate limiting...
func fetchChallengers(lolKey, region string) *lolapi.LeagueDto {
	api := lolapi.NewLolApi(lolKey, region)
	challengers, rerr := api.ChallengerSolo()

	if rerr != nil {

		// handle rate limiting response
		for (rerr != nil &&
            (rerr.StatusCode == 429 || rerr.StatusCode > 500)) ||
            challengers == nil {
			for {
				time.Sleep(time.Second)
				if api.ApiAvailable() {
					break
				}
			}
			challengers, rerr = api.ChallengerSolo()
		}
	}

	return challengers
}

// Header line for output file
func getHeaderLine() string {
	return "region,summonerId,summoner"
}

// Gets formatted string for file output
func getChallengerLine(summoner, id, region string) string {
	var buffer bytes.Buffer
	buffer.WriteString(region)
	buffer.WriteString(",")
	buffer.WriteString(id)
	buffer.WriteString(",")
	buffer.WriteString(summoner)

	return buffer.String()
}

func main() {
	// parse command line options
	regions := ""
	lolKey := ""
	output := ""

	flag.StringVar(&regions, "regions", "na", "Region codes separated by comma")
	flag.StringVar(&lolKey, "lolkey", "", "Riot API key")
	flag.StringVar(&output, "output", "", "Output file to write to")
	flag.Parse()

	if lolKey == "" || output == "" {
		flag.Usage()
		os.Exit(1)
	}

    // create file handle etc...
	fileHandle, err := os.Create(output)
	if err != nil {
		panic(err)
	}

	writer := bufio.NewWriter(fileHandle)
	defer fileHandle.Close()
    fmt.Fprintln(writer, getHeaderLine())

    // fetch the challengers per region
	for _, region := range strings.Split(regions, ",") {
		cleanRegion := strings.TrimSpace(region)

		challengers := fetchChallengers(lolKey, cleanRegion)
		for _, entry := range challengers.Entries {
			playerID := entry.ID
			playerName := entry.Name

            // write out the challengers per region
            fmt.Fprintln(writer, getChallengerLine(playerName, playerID, cleanRegion))
        }
    }
	writer.Flush()
}
