package main

import (
	"fmt"
    "bufio"
    "flag"
    "os"

	"lolapi"
)

func getFileHeader() string {
	return "region,code"
}

func outputToFile(output string) {
    // open output file and create writer
    fileHandle, err := os.Create(output)
    if err != nil {
        panic(err)
    }

    writer := bufio.NewWriter(fileHandle)
    defer fileHandle.Close()
    fmt.Fprintln(writer, getFileHeader())

	api := lolapi.NewLolApi("", "na")
    for _, region := range api.AllDynamicRegion() {
        if region.Code() != "pbe" && region.Code() != "" {
            fmt.Fprintln(writer, fmt.Sprintf("%s,%s", region.Friendly(), region.Code()))
        }
    }
    writer.Flush()
}

func main() {
    // parse command line options
    output := ""

    flag.StringVar(&output, "output", "", "Output file to write to")
    flag.Parse()

    if output == "" {
        flag.Usage()
        os.Exit(1)
    }

    outputToFile(output)
}
