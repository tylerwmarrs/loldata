package main

import (
	"fmt"
	"time"
    "flag"
    "errors"

	"lolapi"
)

// TODO: it might be more logical to check for baron throws before the baron
// event only if there is a near-ace within the pit. This will help avoid
// including things like baron baits and then getting baron after.

// Center x/y values of baron nashor
const BARON_X int64 = 5007
const BARON_Y int64 = 10471

// Radius around baron nashor considered the "baron zone"
const BARON_R int64 = 1947

// Seconds to keep track of kill events before baron was killed
const BEFORE_THRESHOLD int64 = 30

// Seconds to keep track of kill events after baron was killed
const AFTER_THRESHOLD int64 = 30

// LoL api key
const LOL_KEY string = "d1d707f6-496c-47f4-9c13-d69fff225b08"

// Container for a baron event and associated kill events within the before
// and after thresholds
type BaronEvent struct {
	FrameIndex int
	Timestamp  int64
	KillerID   int64
	KillEvents []KillEvent
}

// Provides new instance of BaronEvent
func NewBaronEvent() *BaronEvent {
	be := new(BaronEvent)
	be.KillEvents = make([]KillEvent, 0)
	return be
}

// Adds a KillEvent
func (be *BaronEvent) AddKillEvent(k KillEvent) {
	be.KillEvents = append(be.KillEvents, k)
}

// Keeps track of who killed who, where they were killed and the time they died
type KillEvent struct {
	Timestamp int64
	KillerID  int64
	VictimID  int64
	XCoord    int64
	YCoord    int64
}

// Helper to see if a x,y coord exists within the area of the baron zone
func killInBaronZone(x, y int64) bool {
	dx := BARON_X - x
	dy := BARON_Y - y
	squareDist := (dx * dx) + (dy * dy)
	squareR := BARON_R * BARON_R
	return squareDist <= squareR
}

// Determine what team the parcipant is on. Right now the API has a scale of
// 1 - 10 for IDs. 1-5 = blue, 6-10 = red
func teamForParticipantID(x int64) string {
	if x >= 1 && x <= 5 {
		return "blue"
	}

	return "red"
}

// Convert milliseconds to minutes
func msToM(ms int64) int64 {
	return ms / 1000 / 60
}

// Convert milliseconds to seconds
func msToS(ms int64) int64 {
	return ms / 1000
}

// Check if match contains any baron kills
func baronKilled(match *lolapi.MatchDetailDto) bool {
	return match.Teams[0].BaronKills > 0 ||
		match.Teams[1].BaronKills > 0
}

// Check if match has any baron throws in it
func matchIsThrow(match *lolapi.MatchDetailDto, summonerId uint64) bool {
	if !baronKilled(match) {
		return false
	}
    // find the summoner's team
    summonerTeam := ""
    for _, p := range match.ParticipantIdentities {
        if p.Player.SummonerID == summonerId {
            summonerTeam = teamForParticipantID(p.ParticipantID)
            break
        }
    }

	// Find and keep track of all baron events.
	baronEvents := make([]*BaronEvent, 0)
	for i, f := range match.Timeline.Frames {
		// Baron doesn't spawn until 20 minutes in game so we can skip events
		// earlier events.
		minute := msToM(f.Timestamp)
		if minute < 20 {
			continue
		}

		for _, e := range f.Events {
			if e.MonsterType == "BARON_NASHOR" {
				be := NewBaronEvent()
				be.FrameIndex = i
				be.Timestamp = e.Timestamp
				be.KillerID = e.KillerID
				baronEvents = append(baronEvents, be)
			}
		}
	}

	// Loop over found baron events and find kill events within given time thresholds.
	// We store the kill events found within the threshold.
	for _, be := range baronEvents {

		// Figure out whihc frame index to start and stop at.
		// Each frame specifies a minute interval in time.
		// We only care about events within a short interval of the baron
		// so we only start at one frame before and after.
		startFrameIndex := be.FrameIndex - 1
		endFrameIndex := be.FrameIndex + 1
		if endFrameIndex > len(match.Timeline.Frames) {
			endFrameIndex = len(match.Timeline.Frames) - 1
		}

		for _, f := range match.Timeline.Frames[startFrameIndex:endFrameIndex] {
			for _, e := range f.Events {
				if e.EventType == "CHAMPION_KILL" {
					if e.Timestamp < be.Timestamp {
						secsBefore := msToS(be.Timestamp - e.Timestamp)
						if secsBefore <= BEFORE_THRESHOLD {
							be.AddKillEvent(KillEvent{e.Timestamp, e.KillerID, e.VictimID, e.Position.X, e.Position.Y})
						}
					} else {
						secsAfter := msToS(e.Timestamp - be.Timestamp)
						if secsAfter <= AFTER_THRESHOLD {
							be.AddKillEvent(KillEvent{e.Timestamp, e.KillerID, e.VictimID, e.Position.X, e.Position.Y})
						}
					}
				}
			}
		}
	}

	// Determine if any baron events has "near aces" or aces in them within the
	// baron zone.
	// Essentially - 4 or 5 deaths on either team.
	isThrow := false
	for _, be := range baronEvents {
		blueDeaths := 0
		redDeaths := 0
		for _, ke := range be.KillEvents {
			if killInBaronZone(ke.XCoord, ke.YCoord) {
				victim := teamForParticipantID(ke.VictimID)
				if victim == "blue" {
					blueDeaths++
				} else {
					redDeaths++
				}
			}
		}

		if (summonerTeam == "blue" && blueDeaths >= 4) ||
            (summonerTeam == "red" && redDeaths >= 4) {
			isThrow = true
			break
		}
	}

	return isThrow
}

func main() {
    summonerName := ""
    region := ""
    flag.StringVar(&region, "region", "na", "region code")
    flag.StringVar(&summonerName, "summonerName", "", "summoners name")
    flag.Parse()

    if summonerName == "" {
        panic(errors.New("Expecting summonerName!!!"))
    }

    fmt.Println("Got summoner name: " + summonerName)

	matchSet := make(map[string]bool)
	throwCount := 0
	api := lolapi.NewLolApi(LOL_KEY, "na")
	summoner, err := api.SummonerByName(summonerName)
	time.Sleep(time.Second)

	if err != nil {
		panic(err)
	}

	playerID := fmt.Sprintf("%d", summoner.ID)
	matches, err := api.SummonerRankedMatches(playerID, nil)
	time.Sleep(time.Second)

	if err != nil {
        panic(err)
	}

	for _, match := range matches.Matches {
		matchID := fmt.Sprintf("%d", match.MatchID)
		_, found := matchSet[matchID]
		if !found {
			match, err := api.Match(matchID, true)
			time.Sleep(time.Second)

			if err != nil {
				continue
			}

			matchSet[matchID] = true
			if matchIsThrow(match, summoner.ID) {
				throwCount++
			}

			if len(matchSet)%100 == 0 && len(matchSet) > 0 {
				fmt.Println(fmt.Sprintf("%d observed, %d throws", len(matchSet), throwCount))
			}

		}
	}
	fmt.Println(fmt.Sprintf("%d observed, %d throws", len(matchSet), throwCount))
}
